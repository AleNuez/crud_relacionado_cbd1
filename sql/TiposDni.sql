SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Database: `crud_people`
--

-- --------------------------------------------------------


Create Database IF NOT EXISTS crud_people;

Use crud_people;

CREATE TABLE IF NOT EXISTS `tiposDni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL,
  `abreviatura` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Carga de datos en la tabla tipoDni
--

INSERT INTO `tiposDni` (`tipo`, `abreviatura`) VALUES
('Libreta Civica', 'LC');