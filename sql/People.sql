

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Database: `Crud_People`
--

-- --------------------------------------------------------


Create Database IF NOT EXISTS Crud_People;

Use Crud_People;

CREATE TABLE IF NOT EXISTS `People` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `Street` varchar(100) NOT NULL,
  `Number` int(5) NOT NULL,
  `Flat` varchar(10) ,
  `Departament` varchar(10) ,
  `DateOfBirth` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Carga de datos en la tabla People
--

INSERT INTO `People` (`FirstName`, `LastName`, `Street`,`Number`,`Flat`,`Departament`,`DateOfBirth`) VALUES
('Fabian', 'Lopez','Sarandi','123','','','1973-09-14');


