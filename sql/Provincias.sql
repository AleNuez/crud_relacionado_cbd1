SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Database: `crud_people`
--

-- --------------------------------------------------------


Create Database IF NOT EXISTS crud_people;

Use crud_people;

CREATE TABLE IF NOT EXISTS `provincias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Carga de datos en la tabla provincias
--

INSERT INTO `provincias` (`tipo`, `nombre`) VALUES
('Ciudad Autonoma', 'Ciudad de Buenos Aires');