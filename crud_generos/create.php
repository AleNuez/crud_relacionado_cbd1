<?php
// Include config file
require_once "../config.php";
 
// Define variables and initialize with empty values
$genero = $lastname = "";
$nro = 0;
$genero_err = $lastname_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validar genero
    $input_genero = trim($_POST["genero"]);
    if(empty($input_genero)){
        $genero_err = "Por favor ingrese genero.";
    } elseif(!filter_var($input_genero, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $genero_err = "Por favor ingrese un genero válido";
    } else{
        $genero = $input_genero;
    }

    // verifica los input antes de realizar el inser
    if(empty($genero_err)){
        // Prepare an insert statement
        $sql = "INSERT INTO generos (genero) VALUES (?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_genero); 
            // Set parameters
            $param_genero = ucwords($genero);
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records created successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 800px;
            margin: 100 ;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                 
                <div class="col-md-12">
                    <h2 class="mt-5">Agregar Registro</h2>
                    <p>Por favor complete los siguientes datos</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label>Género</label>
                                <input type="text" name="genero" class="form-control <?php echo (!empty($genero_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $genero; ?>">
                                <span class="invalid-feedback"><?php echo $genero_err;?></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" value="Submit">Grabar </button>
                         <a href="index.php" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>