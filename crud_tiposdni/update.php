<?php
// Include config file
require_once "../config.php";
 
// Define variables and initialize with empty values
$tipo = $abreviatura = "";
$nro = 0;
$tipo_err = $abreviatura_err = "";
 
// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];
    
    // Validar tipo
    $input_tipo = trim($_POST["tipo"]);
    if(empty($input_tipo)){
        $tipo_err = "Por favor ingrese Tipo de DNI.";
    } elseif(!filter_var($input_tipo, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $tipo_err = "Por favor ingrese un Tipo de DNI válido";
    } else{
        $tipo = $input_tipo;
    }
    // Validar abreviatura
     $input_abreviatura = trim($_POST["abreviatura"]);
     if(empty($input_abreviatura)){
         $abreviatura_err = "Por favor ingrese Abreviatura.";     
     } else{
         $abreviatura = $input_abreviatura;
     }

    // Check input errors before updating in database
    if(empty($tipo_err)){
        // Prepare an update statement
        $sql = "UPDATE tiposdni SET tipo=?,abreviatura=? WHERE id=?";
                
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ssi", $param_tipo, $param_abreviatura, $param_id);
            // Set parameters
            $param_tipo = ucwords($tipo);
            $param_abreviatura = strtoupper($abreviatura);
            $param_id = $id;      
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records updated successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Oops!Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
            
        }
           
         // Close statement
        mysqli_stmt_close($stmt);
        
    }
    
    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM tiposdni WHERE id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            
            // Set parameters
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $tipo = $row["tipo"];
                    $abreviatura = $row["abreviatura"];
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    
                    header("location: error.php");
                    exit;
                }
                
            } else{
                echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
        
        // Close connection
        mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
       
        header("location: error.php");
        exit();
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Actualizar Registro</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 800px;
            margin: 100;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <h2 class="mt-5">Actualizar Registro</h2>
                    <p>Por favor complete los siguientes datos</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label>Tipo de DNI</label>
                                <input type="text" name="tipo" class="form-control <?php echo (!empty($tipo_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $tipo; ?>">
                                <span class="invalid-feedback"><?php echo $tipo_err;?></span>
                            </div>
                            <div class="form-group col-md-5">
                                <label>Abreviatura</label>
                                <input name="abreviatura" class="form-control <?php echo (!empty($abreviatura_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $abreviatura; ?>">
                                <span class="invalid-feedback"><?php echo $abreviatura_err;?></span>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id;?>"/>
                        <button type="submit" class="btn btn-primary" value="Submit">Grabar </button>
                         <a href="index.php" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>