<?php
// Include config file
require_once "../config.php";
 
// Define variables and initialize with empty values
$tipo = $abreviatura = "";
$nro = 0;
$tipo_err = $abreviatura_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validar tipo
    $input_tipo = trim($_POST["tipo"]);
    if(empty($input_tipo)){
        $tipo_err = "Por favor ingrese Tipo.";
    } elseif(!filter_var($input_tipo, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $tipo_err = "Por favor ingrese un Tipo de DNI válido";
    } else{
        $tipo = $input_tipo;
    }
    // Validar abreviatura
      $input_abreviatura = trim($_POST["abreviatura"]);
      if(empty($input_abreviatura)){
          $abreviatura_err = "Por favor ingrese Abreviatura.";     
      } else{
          $abreviatura = $input_abreviatura;
      }

    // verifica los input antes de realizar el insert
    if(empty($tipo_err) && empty($abreviatura_err)){
        // Prepare an insert statement
        $sql = "INSERT INTO tiposdni (tipo,abreviatura) VALUES (?,?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_tipo, $param_abreviatura); 
            // Set parameters
            $param_tipo = ucwords($tipo);
            $param_abreviatura = strtoupper($abreviatura);
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records created successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 800px;
            margin: 100 ;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                 
                <div class="col-md-12">
                    <h2 class="mt-5">Agregar Registro</h2>
                    <p>Por favor complete los siguientes datos</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label>Tipo de DNI</label>
                                <input type="text" name="tipo" class="form-control <?php echo (!empty($tipo_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $tipo; ?>">
                                <span class="invalid-feedback"><?php echo $tipo_err;?></span>
                            </div>
                            <div class="form-group col-md-5">
                                <label>Abreviatura</label>
                                <input name="abreviatura" class="form-control <?php echo (!empty($abreviatura_err)) ? 'is-invalid' : ''; ?>"><?php echo $abreviatura; ?>
                                <span class="invalid-feedback"><?php echo $abreviatura_err;?></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" value="Submit">Grabar </button>
                         <a href="index.php" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>