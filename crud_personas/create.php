<?php
// Include config file
require_once "../config.php";
 
// Define variables and initialize with empty values
$name = $lastname = $address = $floor = $dpto= $datenac= "";
$nro = 0;
$name_err = $lastname_err = $address_err = $datenac_err= "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validar nombre
    $input_name = trim($_POST["name"]);
    if(empty($input_name)){
        $name_err = "Por favor ingrese Nombre.";
    } elseif(!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $name_err = "Por favor ingrese un Nombre válido";
    } else{
        $name = $input_name;
    }
    
    // Validar apellido
    $input_lastname = trim($_POST["lastname"]);
    if(empty($input_lastname)){
        $lastname_err = "Por favor ingrese Apellido.";     
    } else{
        $lastname = $input_lastname;
    }
    
    // Validar calle
    $input_address = trim($_POST["address"]);
    if(empty($input_address)){
        $address_err = "Por favor ingrese domicilio.";     
    } else{
        $address = $input_address;
    }
    
    // Validar Fecha de nacimiento
    $input_datenac = trim($_POST["datenac"]);
    if(empty($input_datenac)){
        $datenac_err = "Por favor ingrese una fecha.";     
    } else{
        $datenac = $input_datenac;
    }
    
    $nro=trim($_POST["nro"]);
    $dpto=trim($_POST["dpto"]);
    $floor=trim($_POST["floor"]);

    // verifica los input antes de realizar el inser
    if(empty($name_err) && empty($lastname_err) && empty($address_err) && empty($datenac_err)){
        // Prepare an insert statement
        $sql = "INSERT INTO People (FirstName,LastName,Street,Number,Flat,Departament,DateOfBirth) VALUES (?, ?, ?, ?, ?, ?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssisss", $param_name, $param_lastname, $param_address,$param_nro,$param_floor,$param_dpto,$param_datenac);
            
            // Set parameters
            $param_name = ucwords($name);
            $param_lastname = ucwords($lastname);
            $param_address = ucwords($address);
            $param_nro = $nro;
            $param_floor = $floor;
            $param_dpto = $dpto;
            $param_datenac = $datenac;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records created successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 800px;
            margin: 100 ;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                 
                <div class="col-md-12">
                    <h2 class="mt-5">Agregar Registro</h2>
                    <p>Por favor complete los siguientes datos tal cual figura en su DNI</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label>Nombres</label>
                                <input type="text" name="name" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $name; ?>">
                                <span class="invalid-feedback"><?php echo $name_err;?></span>
                            </div>
                            <div class="form-group col-md-5">
                                <label>Apellidos</label>
                                <input name="lastname" class="form-control <?php echo (!empty($lastname_err)) ? 'is-invalid' : ''; ?>"><?php echo $lastname; ?>
                                <span class="invalid-feedback"><?php echo $lastname_err;?></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-9">
                                <label>Calle</label>
                                <input name="address" class="form-control <?php echo (!empty($address_err)) ? 'is-invalid' : ''; ?>"><?php echo $address; ?>
                                <span class="invalid-feedback"><?php echo $address_err;?></span>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Altura</label>
                                <input type="text" name="nro" class="form-control ">
                               
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Piso</label>
                                <input name="floor" class="form-control ">
                               
                            </div>
                            <div class="form-group col-md-4">
                                <label>Departamento</label>
                                <input type="text" name="dpto" class="form-control ">
                                
                            </div>
                            <div class="form-group col-md-4">
                                <label>Fecha de Nacimiento</label>
                                <input type="date" name="datenac" class="form-control <?php echo (!empty($datenac_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $datenac; ?>">
                                <span class="invalid-feedback"><?php echo $datenac_err;?></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" value="Submit">Grabar </button>
                         <a href="index.php" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>