<?php
// Check existence of id parameter before processing further
if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
    // Include config file
    require_once "../config.php";
    
    // Prepare a select statement
    $sql = "SELECT * FROM People WHERE Id = ?";
    
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        
        // Set parameters
        $param_id = trim($_GET["id"]);
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);
    
            if(mysqli_num_rows($result) == 1){
                /* Fetch result row as an associative array. Since the result set
                contains only one row, we don't need to use while loop */
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                
                // Retrieve individual field value
                $FirstName=$row['FirstName'] ;
                $LastName=$row['LastName'] ;
                $Street=$row['Street'] ;
                $Number=$row['Number'] ;
                $Flat=$row['Flat'] ;
                $Departament=$row['Departament'] ;
                $DateOfBirth=$row['DateOfBirth'] ;

                
            } else{
                // URL doesn't contain valid id parameter. Redirect to error page
                header("location: error.php");
                exit();
            }
            
        } else{
            echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
        }
    }
     
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
} else{
    // URL doesn't contain id parameter. Redirect to error page
    header("location: error.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 800px;
            margin: 100 ;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="mt-5 ">Vista completa de Datos</h1>
                    <div class="form-row">
                    <div class="form-group col-md-7">
                        <label>Apellido</label>
                        <p><b><?php echo $row["LastName"]; ?></b></p>
                    </div>
                  
                    <div class="form-group col-md-5">
                        <label>Nombres</label>
                        <p><b><?php echo $row["FirstName"]; ?></b></p>
                    
                    </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md 9">
                        <label>Calle</label>
                        <p><b><?php echo $row["Street"]; ?></b></p>
                    </div>
                    <div class="form-group col-md 3">
                        <label>Altura</label>
                        <p><b><?php echo $row["Number"]; ?></b></p>
                    </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md 4">
                        <label>Piso</label>
                        <p><b><?php echo $row["Flat"]; ?></b></p>
                    </div>
                    <div class="form-group col-md 4">
                        <label>Dpto.</label>
                        <p><b><?php echo $row["Departament"]; ?></b></p>
                    </div>
                    <div class="form-group col-md 4">
                        <label>Fecha de Nacimiento</label>
                        <p><b><?php echo date("d-m-Y",strtotime($row['DateOfBirth'])); ?></b></p>
                    </div>
                    </div>
                    <p><a href="index.php" class="btn btn-primary">Regresar</a></p>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>



                                       