<?php
// Include config file
require_once "../config.php";
 
// Define variables and initialize with empty values
$name = $localidad = $provincia = "";
$name_err = $localidad_err = $provincia_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validar nombre
    $input_name = trim($_POST["nombre"]);
    if(empty($input_name)){
        $name_err = "Por favor ingrese Nombre.";
    } elseif(!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $name_err = "Por favor ingrese un Nombre válido";
    } else{
        $name = $input_name;
    }
    // Validar localidad
    $input_localidad = trim($_POST["localidad"]);
    if(empty($input_localidad)){
        $localidad_err = "Por favor ingrese Localidad.";     
    } else{
        $localidad = $input_localidad;
    }
    // Validar provincia
    $input_provincia = trim($_POST["provincia"]);
    if(empty($input_provincia)){
        $provincia_err = "Por favor ingrese Provincia.";     
    } else{
        $provincia = $input_provincia;
    }

    // verifica los input antes de realizar el inser
    if(empty($name_err) && empty($localidad_err) && empty($provincia_err)){
        // Prepare an insert statement
        $sql = "INSERT INTO calles (nombre,localidad,provincia) VALUES (?,?,?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sss", $param_name, $param_localidad, $param_provincia); 
            // Set parameters
            $param_name = ucwords($name);
            $param_localidad = ucwords($localidad);
            $param_provincia = ucwords($provincia);
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records created successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 800px;
            margin: 100 ;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                 
                <div class="col-md-12">
                    <h2 class="mt-5">Agregar Registro</h2>
                    <p>Por favor complete los siguientes datos</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Nombre de la calle</label>
                                <input type="text" name="nombre" class="form-control <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $name; ?>">
                                <span class="invalid-feedback"><?php echo $name_err;?></span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Localidad</label>
                                <input type="text" name="localidad" class="form-control <?php echo (!empty($localidad_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $localidad; ?>">
                                <span class="invalid-feedback"><?php echo $localidad_err;?></span>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Provincia</label>
                                <input type="text" name="provincia" class="form-control <?php echo (!empty($provincia_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $provincia; ?>">
                                <span class="invalid-feedback"><?php echo $provincia_err;?></span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" value="Submit">Grabar </button>
                         <a href="index.php" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>