### Descripción

En el repositorio vamos a ver las siguientes ramas

- main : Versión mas estable y presentable del proyecto.
- original: Archivos originales provistos por el docente.
- v1: Ejercicio realizado según requiriemientos.
- v2: Ejercicio optimizado.

### Consigna

Se deja un ejemplo funcional de un Crud con una base de datos .sql, la cual deberán importar
El mismo contiene los ejemplos para realizar las tareas con bootstrap online.-
Se desea que a partir del mismo se generen los siguientes Crud:
Deberán crean una BD que contenga las siguientes tablas:
- Paises
- Provincias
- Generos
- Tipos de DNI
- Calles
Deberán aplicar los conocimientos en clase para luego crear las tablas con el menor grado de redundancia
### Inicio

1. Levantar el proyecto:
    
    - a. Colocar carpeta en htdocs y levantar los servicios de xampp
    - b. Ir a localhost/cruddb1_periodo

    o bien usando docker:

2. Chequear archivo de conexión:
    - a. Server "localhost", nombre de contenedor o nombre dado en hosting
    - b. User / pass (root-root o root y sin pass)
    - c. nombre de la bd

3. Chequear base de datos:
    - a. Ir a phpmyadmin y crear la base de datos o importarla
    - b. Verificar que esté en el archivo de config
    - c. Importar tablas o contenido

